const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const fs = require('fs');
const { db } = require('./config');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));

let routes = require('./routes');
app.use(routes);

global.__basedir = __dirname;
mongoose.Promise = global.Promise;

//
// Connecting with database.
console.log('Connecting to database...');
connectDB()
  .once('connected', ()=>{
    console.log('Database connected');
    // Start the server.
    app.listen(3001, ()=>{
      console.log('Application listening on port:', 3001);
    });
  })
  .on('disconnected', connectDB)
  .on('error', (e)=>{
    console.log(e);
  })
;

function connectDB(){
  console.log(db.url);
  return mongoose.connect(db.url, {
      ssl: db.ssl,
      sslValidate: db.sslValidate,
      sslCA: db.sslCA
  }), mongoose.connection;
}
