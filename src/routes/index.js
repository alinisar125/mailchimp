
let express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


let views = require('./views');
app.use('/', views);

let api = require('./api');
app.use('/api', api);

//
// Route not found.
app.use((req, res, next)=>{
    res.sendStatus(404);
});

//
// Error handler.
app.use((err, req, res, next)=>{
    res.status(500).send(err.message);
})

module.exports = app;
