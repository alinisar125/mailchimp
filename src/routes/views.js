let router = require('express').Router(),
  api = require('../middlewares/mailchimp');


router.route('/subscribe')
  .post(api.saveApiKey)
  .get((req, res) => {
    res
      .status(200)
      .render('form', {
        errorCode: null
      });
  });


module.exports = router;
