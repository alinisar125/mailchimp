let router = require('express').Router(),
  api = require('../middlewares/mailchimp');

router.route('/:api_key')
  .get(api.getProfile);


module.exports = router;
