let mongoose = require('mongoose'),
  { Schema } = require('mongoose');

let profileSchema = new Schema({
  api_key: { type: String, required: true },
  account_id: { type: String },
  login_id: { type: String },
  account_name: { type: String },
  email: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  username: { type: String },
  avatar_url: { type: String },
  role: { type: String },
  contact: {
    company: { type: String },
    addr1: { type: String },
    addr2: { type: String },
    city: { type: String },
    state: { type: String },
    zip: { type: String },
    country: { type: String }
  },
  pro_enabled: { type: Boolean },
  last_login: { type: String },
  total_subscribers: { type: Number },
  industry_stats: {
    open_rate: { type: Number },
    bounce_rate: { type: Number },
    click_rate: { type: Number }
  },
  createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Profile', profileSchema);
