require('dotenv').config();
const Mailchimp = require('mailchimp-api-v3');
const Profile = require('../models/profile.model');
const sortObject = require('sort-object-keys');

require('mongoose').set('debug', true);

/**
 * Authentication of Mailchimp api
 * @param  {String} api_key Client api key
 * @return {Promise}         Returns a mailchimp object or an error
 */
const authenticate = (api_key) => {
  return new Promise((resolve, reject) => {
    if(api_key.length) {
      resolve({
        api_key: api_key,
        mailchimp: new Mailchimp(api_key)
      });
    }
    else {
      reject({
        "message": "Valid API key required."
      });
    }
  });
}

/**
 * Update profile data in db
 * @param  {String} api_key     Api key
 * @param  {Object} profileData Profile data as json document
 * @return {[Promise]}          Update profile data
 */
const updateProfileData = (api_key, profileData) => {
  return new Promise(async (resolve, reject) => {
    await Profile
      .findOne({
        api_key: api_key
      })
      .exec()
      .then(savedProfile => {
        if(savedProfile === null) {
          let newProfile = new Profile(profileData);
          let newProfileSaved = newProfile.save();
          resolve(newProfile);
        }
        else {
          savedProfile = Object.assign(savedProfile, profileData);
          savedProfile.save();
          resolve(savedProfile);
        }
      })
      .catch(mongooseError => {
        reject(mongooseError);
      });
  });
}

/**
 * Update profile information of client
 * @param  {Mailchimp} mailchimp Authenticated mailchimp object
 * @return {Promise}           Return the saved profile data or error
 */
const updateProfile = (api_key, mailchimp) => {
  return new Promise(async (resolve, reject) => {
    await mailchimp.get({
      path : '/'
    })
    .then(async mailchimpResults => {
      mailchimpResults.api_key = api_key;
      await updateProfileData(api_key, mailchimpResults).then(updateProfileData => {
        resolve(updateProfileData);
      })
      .catch(profileUpdateError => {
        reject(profileUpdateError);
      });

    })
    .catch(err => {
      reject(err);
    })
  });
}

/**
 * Get profile by api key
 * @param  {[String]} api_key Api key
 * @return {[void]}
 */
module.exports.getProfile = (req, res, next) => {
  let api_key = req.params.api_key;
  if(api_key === undefined || !api_key.length) {
    res
      .status(401)
      .json({
        "message": "Api key is required"
      });
    return;
  }

  authenticate(api_key).then(authenticated => {
    updateProfile(api_key, authenticated.mailchimp).then(profileData => {
      res
        .status(200)
        .json(profileData);
      return;
    })
    .catch(updateProfileError => {
      res
        .status(401)
        .json(updateProfileError);
      return;
    });

  })
  .catch(authenticationError => {
    res
      .status(401)
      .json(authenticationError);
    return;
  });
};

module.exports.saveApiKey = (req, res, next) => {
  let apiKey = req.body.api_key;
  if(!apiKey.length) {
    res
      .status(400)
      .render('form', {
        errorCode: "apiKeyEmpty"
      });
    return;
  }

  authenticate(apiKey).then(authenticated => {
    updateProfile(apiKey, authenticated.mailchimp).then(profileData => {
      res
        .status(200)
        .render('success', profileData);
      return;
    })
    .catch(updateProfileError => {
      res
        .status(400)
        .render('form', {
          errorCode: "apiKeyWrong"
        });
      return;
    });

  })
  .catch(authenticationError => {
    res
      .status(400)
      .render('form', {
        errorCode: "apiKeyWrong"
      });
  });

}
