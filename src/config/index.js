let fs = require('fs'),
    path = require('path');

require('dotenv').config();

//
// Database configurations.
exports.db = {
    url: process.env.DB_URL,
    useMongoClient: !!process.env.DB_USE_MONGO_CLIENT,
    ssl: !!process.env.DB_SSL,
    sslValidate: !!process.env.DB_SSL_VALIDATE,
    sslCA: !!process.env.DB_SSL_CA_PATH?[ fs.readFileSync(path.join(__basedir, process.env.DB_SSL_CA_PATH)) ]:null
};
