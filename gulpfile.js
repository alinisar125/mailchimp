'use strict';

var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    dotenv = require('dotenv'),
    gutil = require('gulp-util')
;

gulp.task('load-env', function(cb){

    dotenv.config();
    cb();
});

gulp.task('serve-dev', ['load-env'], function(){

    return nodemon({

        script: 'src/index.js',
        watch:
['src/**/*.js','!src/node_modules/**/*','!src/**/*.json']
    });
});

gulp.task('default', ['load-env','serve-dev']);
